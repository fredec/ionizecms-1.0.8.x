<?php
/*
|--------------------------------------------------------------------------
| Ionize Pagination Language file
|
| Copy this file to /themes/<my_theme/language/xx/pagination_lang.php
| to replace these translations with your one.
|
| IMPORTANT:
| Do not modify this file.
| It will be overwritten when migrating to a new Ionize release.
|--------------------------------------------------------------------------
*/
$lang['first_link'] = '<i class="fa fa-angle-double-left"></i>';
$lang['last_link'] = '<i class="fa fa-angle-double-right"></i>';
$lang['next_link'] = '<i class="fa fa-angle-right"></i>';
$lang['prev_link'] = '<i class="fa fa-angle-left"></i>';