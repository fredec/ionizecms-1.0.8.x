<?php
/**
 * Ionize Module lang file
 * Lang terms must always be prefixed by "module_<module_name>"
 * to avoid conflict with core / other translations items
 *
 */
 
$lang['module_dh_emailmanager_title'] = "Contatos VemPraAcademia";
$lang['module_dh_emailmanager_about'] = "Gerenciador de inscrições do programa #VemPraAcademia.";
 
$lang['module_dh_emailmanager_title_edit_email'] = "Edit contact";
$lang['module_dh_emailmanager_email'] = "Email";
$lang['module_dh_emailmanager_name'] = "Name";
$lang['module_dh_emailmanager_language'] = "Language";
 
$lang['module_dh_emailmanager_add_new_email'] = "Add an entry";
$lang['module_dh_emailmanager_label_email'] = "Email";

$lang['module_dh_emailmanager_phone'] = "Phone";
$lang['module_dh_emailmanager_address'] = "Address";
$lang['module_dh_emailmanager_document'] = "Document";

$lang['module_dh_emailmanager_table_heading_email'] = "Email";
$lang['module_dh_emailmanager_table_heading_name'] = "Name";
$lang['module_dh_emailmanager_table_heading_language'] = "File";
$lang['module_dh_emailmanager_table_heading_date'] = "Date";
$lang['module_dh_generate_excel_csv'] = "Generate Excel (CSV) File";


$lang['module_dh_emailmanager_email_already_exists'] = "The inserted email already exists";