<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://doc.ionizecms.com/en/basic-infos/license-agreement
 * @link		http://ionizecms.com
 * @since		Version 0.9.0
 */

// ------------------------------------------------------------------------

/**
 * Ionize Article Model
 *
 * @package		Ionize
 * @subpackage	Models
 * @category	Article
 * @author		Ionize Dev Team
 *
 */

class Article_comment_model extends Base_model 
{

	public $category_table = 			'category';
	public $category_lang_table = 		'category_lang';
	public $article_category_table = 	'article_category';
	public $type_table = 				'article_type';
	public $page_table =				'page';
	public $page_lang_table =			'page_lang';
	public $parent_table =				'page_article';
	public $url_table =					'url';
	public $user_table = 				'users';
	public $menu_table = 				'menu';
	public $tag_table = 				'tag';
	public $tag_join_table = 			'article_tag';

	/* Contains table name which should be used for each filter get.
	 * Purpose : Avoid Ambiguous SQL query when 2 fields have the same name.
	 * ex : 'title' in category and 
	 *
	 */
	private $filter_field_ref = array(
		'title' => 'article_lang',
		'view' => 'page_article'
	);

	/**
	 * Model Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();

		$this->set_table('article');
		$this->set_pk_name('id_article');
		$this->set_lang_table('article_lang');
	}


	// ------------------------------------------------------------------------

	/**
	 * Get one article
	 *
	 * @param	string		$where
	 * @param	string		$lang	Optional. Lang code
	 * @return	array		array
	 */
	public function get($where, $lang = NULL)
	{
		$data = $this->get_lang_list($where, $lang);

		if ( ! empty($data))
			return $data[0];

		return array();
	}


	// ------------------------------------------------------------------------


	/**
	 * Get one article by its ID
	 *
	 * @param	int			$id_article
	 * @param 	null|string	$lang
	 * @return	array
	 */
	public function get_by_id($id_article, $lang = NULL)
	{
		return $this->get(array('article.id_article' => $id_article), $lang);
	}

	// ------------------------------------------------------------------------


	/** 
	 * Get article list with lang data
	 * Used by front-end to get the posts with lang data
	 *
	 * @param	array		$where
	 * @param	string		$lang
	 * @param	bool|string	$filter		SQL filter
	 * @param null $extend_filter
	 * @return	array					Array of articles
	 *
	 */
	public function get_lang_list($where = array(), $lang = NULL, $filter = FALSE, $extend_filter=NULL)
	{
		// Page_Article table
		$this->{$this->db_group}->select($this->parent_table.'.*', FALSE);
		$this->{$this->db_group}->select($this->parent_table.'.online as online_in_page', FALSE);
		$this->{$this->db_group}->join(
			$this->parent_table,
			$this->parent_table.'.id_article = ' .$this->table.'.id_article',
			'left'
		);

		// Page table
		// $this->{$this->db_group}->select('article_list_view, article_view');
		$this->{$this->db_group}->join(
			$this->page_table,
			$this->page_table.'.id_page = ' .$this->parent_table.'.id_page',
			'left'
		);

		// Menu table
		$this->{$this->db_group}->select('menu.id_menu, menu.name as menu_name');
		$this->{$this->db_group}->join(
			$this->menu_table,
			$this->menu_table.'.id_menu = ' .$this->page_table.'.id_menu',
			'left'
		);

		// Default ordering
		if ( empty($where['order_by']))
			$where['order_by'] = $this->parent_table.'.ordering ASC';

		// Correction on $where['id_page']
		if (is_array($where) && isset($where['id_page']) )
		{
			$where[$this->parent_table.'.id_page'] = $where['id_page'];
			unset($where['id_page']);
		}

		// Correction on $where['where_in']
		if (isset($where['where_in']))
		{
			foreach($where['where_in'] as $key => $value)
			{
				if ($key == 'id_page')
				{
					$where['where_in'][$this->parent_table.'.id_page'] = $value;
					unset($where['where_in']['id_page']);
				}
			}
		}

		// Published filter
		$this->filter_on_published(self::$publish_filter, $lang);

		// User's filter (tags)
		if ( $filter !== FALSE)	$this->_set_filter($filter);

		// Add the 'date' field to the query
		$this->{$this->db_group}->select('IF(article.logical_date !=0, article.logical_date, IF(article.publish_on !=0, article.publish_on, article.created )) AS date');

		// Add Type to query
		$this->{$this->db_group}->select($this->type_table.'.type, ' . $this->type_table.'.type_flag');
		$this->{$this->db_group}->join(
			$this->type_table,
			$this->parent_table.'.id_type = ' .$this->type_table.'.id_type',
			'left'
		);

		// Category
		if ( ! empty($where['id_category']))
		{
			$this->_add_category_filter_to_query($where['id_category']);
			unset($where['id_category']);
		}

		// Extend filters
		$this->_add_extend_filter_to_query($extend_filter, $where);

		// Base_model->get_lang_list()
		$articles = parent::get_lang_list($where, $lang);

		// log_message('error', print_r($this->last_query(), TRUE));

		$this->add_categories($articles, $lang);

		$this->add_tags($articles);

		return $articles;
	}

	// ------------------------------------------------------------------------


	/**
	 * Adds Published filtering on articles get_lang_list() call
	 *
	 * @param   bool    $on
	 * @param   string  $lang
	 */
	protected function filter_on_published($on = TRUE, $lang = NULL)
	{
		if ($on === TRUE)
		{
			$this->{$this->db_group}->where($this->parent_table.'.online', '1');
	
			if ($lang !== NULL && count(Settings::get_online_languages()) > 1)
				$this->{$this->db_group}->where($this->lang_table.'.online', '1');		
	
			$this->{$this->db_group}->where('((article.publish_off > ', 'now()', FALSE);
			$this->{$this->db_group}->or_where('article.publish_off = ', '0)' , FALSE);
		
			$this->{$this->db_group}->where('(article.publish_on < ', 'now()', FALSE);
			$this->{$this->db_group}->or_where('article.publish_on = ', '0))' , FALSE);
		}	
	}

	// ------------------------------------------------------------------------


	/**
	 * Adds the 'categories' array to each passed article in the $artices array
	 *
	 * @param array			$articles
	 * @param null|string	$lang
	 */
	public function add_categories(&$articles = array(), $lang = NULL)
	{
		// Add Categories to each article
		$categories = $art_cat = array();

		$this->{$this->db_group}->join($this->category_lang_table, $this->category_table.'.id_category = ' .$this->category_lang_table.'.id_category', 'left');

		if ( ! is_null($lang))
			$this->{$this->db_group}->where($this->category_lang_table.'.lang', $lang);

		$query = $this->{$this->db_group}->get($this->category_table);

		if($query->num_rows() > 0)
		{
			$categories = $query->result_array();

			// Get categories articles table content
			$query = $this->{$this->db_group}->get($this->article_category_table);

			// table of links between articles and categories
			if($query->num_rows() > 0) $art_cat = $query->result_array();
		}

		// Add entry to each data array element
		foreach ($articles as $key => $article)
		{
			$articles[$key]['categories'] = array();

			if ( ! empty($categories))
			{
				foreach($art_cat as $cat)
				{
					if($articles[$key]['id_article'] == $cat['id_article'])
					{
						foreach($categories as $c)
						{
							if ($c['id_category'] == $cat['id_category'])
								$articles[$key]['categories'][] = $c;
						}
					}
				}
			}
		}
	}


	// ------------------------------------------------------------------------


	/**
	 * @param array $articles
	 */
	public function add_tags(&$articles = array())
	{
		// Add Tags to each article
		$tags = array();

		$articles_ids = array();
		foreach($articles as $article)
			$articles_ids[] = $article['id_article'];

		if ( ! empty($articles_ids))
		{
			$this->{$this->db_group}->join(
				$this->tag_join_table,
				$this->tag_join_table.'.id_tag = ' .$this->tag_table.'.id_tag', 'inner');

			$this->{$this->db_group}->where_in($this->tag_join_table.'.id_article', $articles_ids);

			$this->{$this->db_group}->select(
				$this->tag_table.'.id_tag, ' .
				$this->tag_join_table.'.id_article, ' .
				$this->tag_table.'.tag_name,' .
				$this->tag_table.'.tag_name as title'
			);

			$query = $this->{$this->db_group}->get($this->tag_table);

			if($query->num_rows() > 0)
				$tags = $query->result_array();
		}

		// Add entry to each data array element
		foreach ($articles as $key => $article)
		{
			$articles[$key]['tags'] = array();

			if ( ! empty($tags))
			{
				foreach($tags as $tag)
				{
					if($articles[$key]['id_article'] == $tag['id_article'])
					{
						$articles[$key]['tags'][] = $tag;
					}
				}
			}
		}
	}

	protected function _add_extend_filter_to_query($filters, &$query_where=array())
	{
		$order_by = ! empty($query_where['order_by']) ? trim($query_where['order_by'], " ,") : NULL;
		$json = json_decode($filters, TRUE);

		if (empty($filters))
		{
			return;
		}

		// JSON filters
		if ($json != FALSE)
		{
			if (isset($json['extends'])) $json = array($json);

			foreach ($json as $idx_filter => $filter)
			{
				if ( ! empty($filter['extends']))
				{
					$where = str_replace('.gt', '>', $filter['expression']);
					$where = str_replace('.lt', '<', $where);
					$where = str_replace('.eq', '=', $where);
					$where = str_replace('.neq', '!=', $where);

					if (! is_array($filter['extends'])) $filter['extends'] = array($filter['extends']);

					foreach($filter['extends'] as $idx_extend => $extend_name)
					{
						$idx = $idx_filter . $idx_extend;

						$extend = self::$ci->extend_field_model->get_extend_definition_from_name(trim($extend_name));

						if ( ! is_null($extend))
						{
							$where = str_replace($extend['name'], 'efs_' . $idx . '.content', $where);
						}

						$this->{$this->db_group}->select('efs_' . $idx . '.content as extend_' . $extend['id_extend_field']);

						$this->{$this->db_group}->join(
							'extend_fields efs_' . $idx,
							"efs_" . $idx . ".id_extend_field = " . $extend['id_extend_field'] .
							" AND efs_" . $idx . ".parent = 'article'" .
							" AND efs_" . $idx . ".id_parent = article.id_article",
							'left'
						);

						// Order by
						if ( ! is_null($order_by))
						{
							$attrs = explode(',', $order_by);

							foreach($attrs as $attr_i => $attr)
							{
								if ( ! empty($attr))
								{
									$arr = preg_split("/[\s*]/i", trim($attr));

									if (trim(strtolower($arr[0])) === $extend['name'])
									{
										$_order = str_replace($arr[0], "efs_" . $idx . ".content", $attr);

										$this->{$this->db_group}->order_by($_order, NULL, FALSE);
										unset($attrs[$attr_i]);
									}
								}
								else
									unset($attrs[$attr_i]);
							}

							$order_by = implode(',', $attrs);
						}

						if (empty($order_by))
							unset($query_where['order_by']);
						else
							$query_where['order_by'] = $order_by;
					}

					$this->{$this->db_group}->where($where, '', false);
				}
			}
		}
		else
		{
			// trace('WTF !!! ');
			// trace($filters);

			$filters = ! is_array($filters) ? explode(';', $filters) : $filters;

			foreach ($filters as $idx => $filter)
			{
				$filter = str_replace('.gt', '>', $filter);
				$filter = str_replace('.lt', '<', $filter);
				$filter = str_replace('.eq', '=', $filter);
				$filter = str_replace('.neq', '!=', $filter);

				$matches = array();
				$test = preg_match("/(.*)([=<>])(.*)/", $filter, $matches);

				if ($test === 1)
				{
					$extend = self::$ci->extend_field_model->get_extend_definition_from_name(trim($matches[1]));

					if ( ! is_null($extend))
					{
						$where = str_replace(trim($matches[1]), 'efs_' . $idx . '.content', $matches[0]);

						$this->{$this->db_group}->select('efs_' . $idx . '.content as extend_' . $extend['id_extend_field']);

						$this->{$this->db_group}->join(
							'extend_fields efs_' . $idx,
							"efs_" . $idx . ".id_extend_field = " . $extend['id_extend_field'] .
							" AND efs_" . $idx . ".parent = 'article'" .
							" AND efs_" . $idx . ".id_parent = article.id_article",
							'left'
						);

						$this->{$this->db_group}->where($where, '', false);

						// Query Where
						if ( ! is_null($order_by))
						{
							$attrs = explode(',', $order_by);

							foreach($attrs as $attr_i => $attr)
							{
								if ( ! empty($attr))
								{
									$arr = preg_split("/[\s*]/i", trim($attr));

									if (trim(strtolower($arr[0])) === $extend['name'])
									{
										$_order = str_replace($arr[0], "efs_" . $idx . ".content", $attr);

										$this->{$this->db_group}->order_by($_order, NULL, FALSE);
										unset($attrs[$attr_i]);
									}
								}
								else
									unset($attrs[$attr_i]);
							}

							$order_by = implode(',', $attrs);
						}

						if (empty($order_by))
							unset($query_where['order_by']);
						else
							$query_where['order_by'] = $order_by;
					}
					else
					{
						unset($query_where['order_by']);
					}
				}
				else
				{
					// Savety : Remove order_by
					unset($query_where['order_by']);
				}
			}
		}
	}


	protected function _add_category_filter_to_query($id_category)
	{
		if ( ! empty($id_category))
		{
			$this->{$this->db_group}->join(
				$this->article_category_table,
				$this->article_category_table . '.id_article = ' . $this->table . '.id_article AND ' . $this->article_category_table . '.id_category =' . $id_category
			);
		}
	}
}